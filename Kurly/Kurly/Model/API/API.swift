//
//  API.swift
//  Kurly
//
//  Created by Hanamobile on 2021/06/16.
//

import Foundation
import Alamofire


class API{
    static func topMostController() -> UIViewController
    {
        var topController = UIApplication.shared.keyWindow?.rootViewController
        while topController?.presentedViewController != nil {
            if !((topController?.presentedViewController?.isKind(of: UIAlertController.self))!)
            {
                topController = topController?.presentedViewController
            }
        }
        
        if topController == nil
        {
            let topWindow:UIWindow? = UIWindow(frame:UIScreen.main.bounds)
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindow.Level.alert + 1
            topWindow?.makeKeyAndVisible()
            
            topController = topWindow?.rootViewController
        }
        
        return topController!
    }
    
    static func showAlert(contents:String?){
        
        let alert = UIAlertController.init(title: "알림", message: contents, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "확인", style: .default, handler: { _ in
            
        }))
        topMostController().present(alert, animated: true, completion: nil)
        
    }
    
    //Genric 함수. (response, Type) -> Decodable(Type)
    static func isCheckResp<T>(_ response:AFDataResponse<Data>, type:T.Type) throws -> T? where T : Decodable{
        
        switch response.result{
        
        case .success(let data):
            switch response.response?.statusCode{
            case 200:
                if let info = try? JSONDecoder().decode(type, from: data){
                    return info
                }
            case 304:
                if let error_info = try? JSONDecoder().decode(Git_Errors_Info.self, from: data){
                    print(error_info)
                        self.showAlert(contents: error_info.message)
                }else{
                        self.showAlert(contents: "Status: 304 Not Modified")
                }
            case 422:
                if let error_info = try? JSONDecoder().decode(Git_Errors_Info.self, from: data){
                        self.showAlert(contents: error_info.message)
                }else{
                        self.showAlert(contents: "Status: 422 Unprocessable Entity")
                }
            case 503:
                if let error_info = try? JSONDecoder().decode(Git_Errors_Info.self, from: data){
                        self.showAlert(contents: error_info.message)
                }else{
                        self.showAlert(contents: "Status: 503 Service Unavailable")
                }
                break
            default:
                    self.showAlert(contents: "네트워크에 문제가 발생했습니다. 잠시후에 다시 시도해주세요.")
                break
            }

        case .failure(let err):
                            self.showAlert(contents: "네트워크에 문제가 발생했습니다. 잠시후에 다시 시도해주세요. \(err.localizedDescription)")
            break
        }
        
        return nil
    }

    /*
      Repositories 데이터를 수신
     */
    static func requestGet(searchText:String?, completion:@escaping (Git_Repositories_Info?) -> Void) {
        let url = Config.URL.Repositories
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        
        let headers:HTTPHeaders = ["Accept":"application/vnd.github.v3+json"]
        
        if searchText?.count ?? 0 < 1{
            showAlert(contents: "검색어를 입력해주세요.")
            return
        }
        
        AF.request(url, method: .get, parameters: ["q":searchText ?? "", "page" : 0], headers: headers)
            .responseData { response in
                if let info = try? isCheckResp(response, type: Git_Repositories_Info.self){
                    completion(info)
                }else{
                    completion(nil)
                }
            }
    }

}
