//
//  Structs.swift
//  Kurly
//
//  Created by Hanamobile on 2021/06/15.
//

import Foundation


/**
 Repositorie 검색시 받아오는 정보
 참고 : https://docs.github.com/en/rest/reference/search#search-repositories
 */

struct Response:Codable{
    let success:Bool
    let result:String
    let message:String
    
}
struct Git_Errors_Info : Codable {
    var documentation_url:String?
    var errors:[Error_Info]?
    var message:String?
}
struct Error_Info : Codable {
    var code: String? // = missing;
    var field:String? // = q;
    var resource:String? // = Search;
}
struct Repositories_Info:Codable {
    var id : Double? //": 43320624,
    var node_id : String? //": "MDEwOlJlcG9zaXRvcnk0MzMyMDYyNA==",
    var name : String? //": "search",
    var full_name : String? //": "lapism/search",
    //    var private : Bool? //": false,
    var html_url : String? //": "https://github.com/lapism/search",
    var description : String? //": "Material Design Search component for Android, SearchView",
    var fork : Bool? //": false,
    var url : String? //": "https://api.github.com/repos/lapism/search",
    var forks_url : String? //": "https://api.github.com/repos/lapism/search/forks",
    var keys_url : String? //": "https://api.github.com/repos/lapism/search/keys{/key_id}",
    var collaborators_url : String? //": "https://api.github.com/repos/lapism/search/collaborators{/collaborator}",
    var teams_url : String? //": "https://api.github.com/repos/lapism/search/teams",
    var hooks_url : String? //": "https://api.github.com/repos/lapism/search/hooks",
    var issue_events_url : String? //": "https://api.github.com/repos/lapism/search/issues/events{/number}",
    var events_url : String? //": "https://api.github.com/repos/lapism/search/events",
    var assignees_url : String? //": "https://api.github.com/repos/lapism/search/assignees{/user}",
    var branches_url : String? //": "https://api.github.com/repos/lapism/search/branches{/branch}",
    var tags_url : String? //": "https://api.github.com/repos/lapism/search/tags",
    var blobs_url : String? //": "https://api.github.com/repos/lapism/search/git/blobs{/sha}",
    var git_tags_url : String? //": "https://api.github.com/repos/lapism/search/git/tags{/sha}",
    var git_refs_url : String? //": "https://api.github.com/repos/lapism/search/git/refs{/sha}",
    var trees_url : String? //": "https://api.github.com/repos/lapism/search/git/trees{/sha}",
    var statuses_url : String? //": "https://api.github.com/repos/lapism/search/statuses/{sha}",
    var languages_url : String? //": "https://api.github.com/repos/lapism/search/languages",
    var stargazers_url : String? //": "https://api.github.com/repos/lapism/search/stargazers",
    var contributors_url : String? //": "https://api.github.com/repos/lapism/search/contributors",
    var subscribers_url : String? //": "https://api.github.com/repos/lapism/search/subscribers",
    var subscription_url : String? //": "https://api.github.com/repos/lapism/search/subscription",
    var commits_url : String? //": "https://api.github.com/repos/lapism/search/commits{/sha}",
    var git_commits_url : String? //": "https://api.github.com/repos/lapism/search/git/commits{/sha}",
    var comments_url : String? //": "https://api.github.com/repos/lapism/search/comments{/number}",
    var issue_comment_url : String? //": "https://api.github.com/repos/lapism/search/issues/comments{/number}",
    var contents_url : String? //": "https://api.github.com/repos/lapism/search/contents/{+path}",
    var compare_url : String? //": "https://api.github.com/repos/lapism/search/compare/{base}...{head}",
    var merges_url : String? //": "https://api.github.com/repos/lapism/search/merges",
    var archive_url : String? //": "https://api.github.com/repos/lapism/search/{archive_format}{/ref}",
    var downloads_url : String? //": "https://api.github.com/repos/lapism/search/downloads",
    var issues_url : String? //": "https://api.github.com/repos/lapism/search/issues{/number}",
    var pulls_url : String? //": "https://api.github.com/repos/lapism/search/pulls{/number}",
    var milestones_url : String? //": "https://api.github.com/repos/lapism/search/milestones{/number}",
    var notifications_url : String? //": "https://api.github.com/repos/lapism/search/notifications{?since,all,participating}",
    var labels_url : String? //": "https://api.github.com/repos/lapism/search/labels{/name}",
    var releases_url : String? //": "https://api.github.com/repos/lapism/search/releases{/id}",
    var deployments_url : String? //": "https://api.github.com/repos/lapism/search/deployments",
    var created_at : String? //": "2015-09-28T19:00:30Z",
    var updated_at : String? //": "2021-06-13T20:04:25Z",
    var pushed_at : String? //": "2021-05-23T18:51:13Z",
    var git_url : String? //": "git://github.com/lapism/search.git",
    var ssh_url : String? //": "git@github.com:lapism/search.git",
    var clone_url : String? //": "https://github.com/lapism/search.git",
    var svn_url : String? //": "https://github.com/lapism/search",
    var homepage : String? //": "",
    var size : Int? //": 118,
    var stargazers_count : Int? //": 2163,
    var watchers_count : Int? //": 2163,
    var language : String? //": "Kotlin",
    var has_issues : Bool? //": true,
    var has_projects : Bool? //": true,
    var has_downloads : Bool? //": true,
    var has_wiki : Bool? //": true,
    var has_pages : Bool? //": false,
    var forks_count : Double? //": 372,
    var mirror_url : String? //": null,
    var archived : Bool? //": false,
    var disabled : Bool? //": false,
    var open_issues_count : Int? //": 1,
    var forks : Int? //": 372,
    var open_issues : Int? //": 1,
    var watchers : Int? //": 2163,
    var default_branch : String? //": "master_v1",
    var score : Double? //": 1.0
    
    var license: License_Info? //해당 정보는 현재 사용하지 않는것이라 판단 별도 선언하지 않음
//    var owner:String? //해당 정보는 현재 사용하지 않는것이라 판단 별도 선언하지 않음

}
struct Git_Repositories_Info : Codable{
    
    var total_count:Int? //": 40,
    var incomplete_results:Bool? //": false,
    var items:[Repositories_Info]?
}
struct License_Info : Codable{
    
    var key:String?//": "apache-2.0",
    var name:String? //": "Apache License 2.0",
    var spdx_id:String? //": "Apache-2.0",
    var url:String? //": "https://api.github.com/licenses/apache-2.0",
    var node_id:String? //": "MDc6TGljZW5zZTI="
    
}
