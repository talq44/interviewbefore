//
//  Extensions.swift
//  Kurly
//
//  Created by Hanamobile on 2021/06/15.
//

import Foundation
import UIKit

extension UITableView{
  
    func registCell(name:String){
        self.register(UINib.init(nibName: name, bundle: nil), forCellReuseIdentifier: name)
    }
}
