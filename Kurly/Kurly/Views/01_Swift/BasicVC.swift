//
//  BasicVC.swift
//  Kurly
//
//  Created by Hanamobile on 2021/06/15.
//

import UIKit
import Alamofire

class BasicVC: BaseVC {

    // ------------------------------------------
    // MARK: - Outlet
    // ------------------------------------------
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var tableViewList: UITableView!
    var arrList:[Repositories_Info] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initData()
        initView()
    }
    
    ///데이터 초기화
    func initData(){
        tableViewList.registCell(name: "RepositoriesListItemCell")
    }
    ///화면 초기화
    func initView(){
        title = "swift 기본형"
    }
    
    ///검색버튼
    @IBAction func selectBtnSearch(_ sender: Any) {
        requestGet()
    }
    
    func showAlert(contents:String?){
        
        let alert = UIAlertController.init(title: "알림", message: contents, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "확인", style: .default, handler: { _ in
            
        }))
        present(alert, animated: true, completion: nil)
    }
}
// ------------------------------------------
// MARK: - UITextFiled Delegate
// ------------------------------------------
extension BasicVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text?.count ?? 0 > 0 {
            requestGet()
        }
        return true
    }
}

// ------------------------------------------
// MARK: - Network
// ------------------------------------------
extension BasicVC {
    func requestGet() {
        view.endEditing(true)
        
        API.requestGet(searchText: tfSearch.text) { [weak self] info in
            guard let self = self else { return }
            if let info = info{
                self.arrList = info.items ?? []
                self.tableViewList.reloadData()
            }
        }
    }
}

// ------------------------------------------
// MARK: - Tableview Delegate/Datasource
// ------------------------------------------
extension BasicVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoriesListItemCell", for: indexPath) as? RepositoriesListItemCell{
            cell.setDataToView(info: arrList[indexPath.row])
            return cell
        }
        return UITableViewCell.init()
    }
    
    
}
