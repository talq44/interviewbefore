//
//  RepositoriesListItemCell.swift
//  Kurly
//
//  Created by Hanamobile on 2021/06/15.
//

import UIKit

class RepositoriesListItemCell: UITableViewCell {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDescription: UILabel!
    @IBOutlet weak var lbMoreInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    func setDataToView(info:Repositories_Info?){
    
        if let info = info{
            lbTitle.text = info.full_name
            lbDescription.text = info.description
            let moreInfo = "\(info.watchers_count ?? 0)  \(info.language ?? "")  \(info.license?.name ?? "")"
            lbMoreInfo.text = moreInfo
        }
    }
}
