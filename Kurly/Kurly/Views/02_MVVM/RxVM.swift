//
//  RxVM.swift
//  Kurly
//
//  Created by Hanamobile on 2021/06/15.
//

import Foundation
import RxSwift
import RxCocoa

class RxVM{
    
    let searchText:Observable<String>
    
    let disposeBag:DisposeBag = .init()
    
    let list:Observable<[Repositories_Info?]>
    let listData:BehaviorSubject<[Repositories_Info?]> = .init(value: [])
    
    deinit {
        
    }
    
    init(search:Observable<String>, // 검색어
         selectBtn:Observable<Void>, // 버튼 선택
         selectDone:Observable<()> // 키보드 Done 선택
    ){
        searchText = search
        
        list = listData.asObservable()
        
        let _ = Observable.of(selectBtn, selectDone).merge()
            .withLatestFrom(search)
            .subscribe {[weak self] text in
                guard let self = self else { return }
                API.requestGet(searchText: text) { info in
                    self.listData.onNext(info?.items ?? [])
                }
            } onError: { err in
                
            }.disposed(by: disposeBag)
        
    }
}
