//
//  RxVC.swift
//  Kurly
//
//  Created by Hanamobile on 2021/06/15.
//

import UIKit
import RxSwift
class RxVC: BaseVC {

    // --------------------------------------------------
    // MARK: - Outlet
    // --------------------------------------------------
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var tableViewList: UITableView!
    
    // --------------------------------------------------
    // MARK: - Value Definition
    // --------------------------------------------------
    var vm:RxVM?
    let bag:DisposeBag = .init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initData()
        initView()
        initBind()
    }
    
    deinit {
        vm = nil
    }
    
    func initData(){
        tableViewList.registCell(name: "RepositoriesListItemCell")
    }
    func initView(){
        title = "RxSwift + MVVM"
    }
    func initBind(){
        
        vm = .init(search: tfSearch.rx.text.orEmpty.asObservable(), selectBtn: btnSearch.rx.tap.asObservable(), selectDone: tfSearch.rx.controlEvent(.editingDidEndOnExit).asObservable())
        
        vm?.list.bind(to: tableViewList.rx.items(cellIdentifier: "RepositoriesListItemCell", cellType: RepositoriesListItemCell.self)){ index, item, cell in
            cell.setDataToView(info: item)
        }.disposed(by: bag)
    }

}
